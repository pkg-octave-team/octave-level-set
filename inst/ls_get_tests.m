##  Copyright (C) 2013-2019  Daniel Kraft <d@domob.eu>
##  GNU Octave level-set package.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn  {Function File} {@var{phis} =} ls_get_tests ()
## 
## Return a couple of level-set functions for certain 2D cases in the
## cell-array @var{phis}.  They can be used in tests.
##
## Use @code{demo ls_get_tests} to get an overview of the situations included.
##
## @end deftypefn

function phis = ls_get_tests ()
  phis = {};

  % The most basic situation.
  phi = ones (3, 3);
  phi(2, 2) = -1;
  phis{end + 1} = phi;

  % The same but now with infinite values.
  phi = Inf (3, 3);
  phi(2, 2) = -Inf;
  phis{end + 1} = phi;

  % Include a fat zero level-set.
  phi = ones (6, 6);
  phi(2 : 5, 2 : 5) = 0;
  phi(3, 4) = -1;
  phis{end + 1} = phi;

  % Two connected components.
  x = linspace (-10, 10, 100);
  y = linspace (-10, 10, 80);
  [XX, YY] = meshgrid (x, y);
  phi = ls_union ((XX - 2).^2 + (YY - 5).^2 - 3^2, ...
                  (XX + 2).^2 + (YY - 5).^2 - 3^2, ...
                  XX.^2 + (YY + 3).^2 - 1.5^2);
  phis{end + 1} = phi;

  % Include a "narrow pair".
  phi = ones (6, 6);
  phi(2 : 3, 4 : 5) = -1;
  phi(4 : 5, 2 : 3) = -1;
  phis{end + 1} = phi;

  % Very small value which may be rounded to zero during
  % operations like ls_signed_distance.
  phi = ones (4, 4);
  phi(2 : 3, 2 : 3) = -1;
  phi(2, 2) = -1e-18;
  phis{end + 1} = phi;
endfunction

% Demo function that plots all the tests.
%!demo
%!  phis = ls_get_tests ();
%!  for i = 1 : length (phis)
%!    figure ();
%!    hold ("on");
%!    imagesc (sign (phis{i}));
%!    axis ("equal");
%!    caxis ([-1, 1]);
%!    hold ("off");
%!    titleFmt = "Situation #%d.  Blue: Interior, Green: Zero, Red: Outside.";
%!    title (sprintf (titleFmt, i));
%!  endfor

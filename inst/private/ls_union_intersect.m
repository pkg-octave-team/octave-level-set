##  Copyright (C) 2014  Daniel Kraft <d@domob.eu>
##  GNU Octave level-set package.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn  {Function File} {@var{phi} =} ls_union_intersect (@var{fcn}, @var{phis}, @var{fcnName})
## 
## Internal routine to calculate intersection or union.  @var{fcn} should be
## a handle to either @code{min} or @code{max}.  @var{phis} is a cell-array
## of level-set functions.
##
## @seealso{ls_union, ls_intersect}
## @end deftypefn

function res = ls_union_intersect (fcn, phis, fcnName)
  if (length (phis) < 1)
    print_usage (fcnName);
  endif

  sz = size (phis{1});
  for i = 2 : length (phis)
    if (!all (size (phis{i}) == sz))
      error ("size mismatch in the arguments");
    endif
  endfor

  res = phis{1};
  for i = 2 : length (phis)
    res = fcn (res, phis{i});
  endfor
endfunction

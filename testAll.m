##  Copyright (C) 2014-2015  Daniel Kraft <d@domob.eu>
##  GNU Octave level-set package.
##
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Main program for running all the tests.

clear ("all");
page_screen_output (false);
addpath ("src");
addpath ("inst");

warning ("off", "level-set:fast-marching:increased-distance");
warning ("off", "level-set:fast-marching:too-far-alive");

test fastmarching

test ls_init_narrowband
test ls_distance_fcn
test ls_signed_distance
test ls_hausdorff_dist

test ls_solve_stationary
test ls_extract_solution

test upwind_gradient_norm
test ls_time_step

test ls_check
test ls_enforce
test ls_enforce_speed

test ls_inside
test ls_issubset
test ls_isempty
test ls_equal
test ls_disjoint

test ls_genbasic
test ls_complement
test ls_union
test ls_intersect
test ls_setdiff
test ls_setxor

test ls_find_geometry
test ls_absolute_geom
test ls_nb_from_geom
test ls_build_mesh

test ls_normalise
test ls_sign_colourmap

test so_init_params
test so_step_armijo
test so_run_descent
test so_save_descent
test so_replay_descent
test so_explore_descent
test so_example_problem

% Now test private functions.
addpath ("inst/private");
test ls_copy_sign
